/* 
Header file for Date class 
*/

#pragma once // this to avoid including this header multiple times (when included by other headers)

#include<string>

class Date {
        public:
                unsigned int day;
                std::string month;
                unsigned int year;

                Date();
};
