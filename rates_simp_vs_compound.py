import matplotlib
from matplotlib import pyplot as plt
import numpy as np

'''
Compare accumulations by compound or simple interest, for a family of interest rates
'''

# parameters
T = 2 # maturity / year
N = 1000 # time steps
rates = [0.01, 0.05, 0.25, 0.5, 0.65, 0.90] # annual interest rates

# accumulation functions
def simple(_t,_mu):
	return (1+_mu*_t)
def compound(_t,_mu):
	return (1+_mu)**(_t) 

# time span / year
tt = np.linspace(0,T,N)

# plotting differences
for mu in rates:
	plt.plot(tt, compound(tt,mu) - simple(tt,mu), label="mu = "+str(mu))
	plt.ion()
plt.plot(tt,np.zeros(tt.shape),'k--')
plt.xlabel("time / year")
plt.ylabel("normalized diff of ROIs")
plt.title("Comparison compound vs simple interest")
plt.legend()
plt.savefig("rates_compound_vs_simple.png")

plt.close()
