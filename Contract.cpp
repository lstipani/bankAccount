#include<iostream>
#include<string>
#include"Party.h"
#include"Contract.h"
#include"Date.h"


/* Contract methods */
Contract::Contract(Person _u, Institution _p):
	user(_u), provider(_p)
	{
	}
	
Person Contract::getUser()
	{
	return user;
	}
		
Institution Contract::getProvider()
	{
	return provider;
	}	


/* BankAccount methods */
BankAccount::BankAccount(Person _u, Institution _p):
	Contract(_u, _p), balance(0.0)
	{
	}


/* Functions */

void check(Contract x){
	std::cout << x.getProvider().getName() << " has a liability towards " << x.getUser().getName() << std::endl;
};

