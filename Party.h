/*
Header for class Party and its derived ones.
Party: Person (e.g. myself) or Institution (e.g. a bank)
*/

#pragma once // this to avoid including this header multiple times (when included by other headers)

#include<string>
#include"Date.h"

class Party {

	protected:
		std::string name;
		std::string address;

	public:
		Party(std::string _n, std::string _a);
		
		std::string getName();
};

class Person: public Party{

	Date birth;

	public:
		Person(std::string _n, std::string _b, std::string _a);
};

class Institution: public Party{

	std::string swift;
	
	public:
		Institution(std::string _n, std::string _a, std::string _s);
};
