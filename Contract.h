/*
Header for class Contract and its derived one.
*/

#pragma once // this to avoid including this header multiple times (when included by other headers)

#include<iostream>
#include<string>
#include"Party.h"
#include"Date.h"


/* Contract: an agreement between Parties (e.g. BankAccount) */
class Contract{

	protected: 
		Person user;
		Institution provider;
	
	public:
		Contract(Person _u, Institution _p);
		
		Person getUser();
		
		Institution getProvider();
};


/* BankAccount */
class BankAccount: public Contract{
	
	float balance;
	
	public:
		BankAccount(Person _u, Institution _p);
};


/* Functions */

// check: print out whether user is connected to provider
void check(Contract x);

