/*
Definition of class Party and its derived ones.
Party: Person (e.g. myself) or Institution (e.g. a bank)
*/

#include"Party.h"
#include"Date.h"

/* Party methods */
Party::Party(std::string _n, std::string _a): 
	name(_n), address(_a) { 
}

std::string Party::getName(){
	return this->name;
}


/* Person methods */
Person::Person(std::string _n, std::string _b, std::string _a):
	Party(_n, _a) {
	birth.day   = std::stoul(_b.substr(0,2));
	birth.month = _b.substr(3,3);
	birth.year  = std::stoul(_b.substr(7,4));
}
	

/* Institution methods */	
Institution::Institution(std::string _n, std::string _a, std::string _s):
	Party(_n, _a), swift(_s) {
}

